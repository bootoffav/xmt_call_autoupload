<?php

declare(strict_types=1);


namespace bootoffav\XMT\call_autoupload;

class SourceFile
{
    public $dir;
    public $name;
    public $call_direction;
    public $external_number;
    public $internal_number;
    public $called_date;
    public $duration = [];

    public function __construct(string $dir, string $filename, $call_duration_in_sec)
    {
        $this->dir = $dir;
        $this->name = $filename;
        $this->duration['min'] = intdiv($call_duration_in_sec, 60);
        $this->duration['sec'] = $call_duration_in_sec % 60;
        $filename_parts = explode('-', $filename);
        if ($filename_parts[0] === 'out') {
            $this->call_direction = '2';
            $this->external_number = substr($filename_parts[1], 0, 2) == '00' ? substr($filename_parts[1], 2) : $filename_parts[1];
            $this->internal_number = $filename_parts[2];
            $this->called_date = $filename_parts[3];
            $this->called_time = $filename_parts[4];
        }
        if ($filename_parts[0] === 'in') {
            $this->call_direction = '1';
            if ($filename_parts[1] == '37034720299') {
                $this->internal_number = '862';
                $this->external_number = substr($filename_parts[2], 2);
            }
           if ($filename_parts[1] == '575203') {
                $this->internal_number = '729';
                $this->external_number = substr($filename_parts[2], 1);
            }
            if ($filename_parts[1] == '730072') {
                $this->internal_number = '853';
                $this->external_number = substr($filename_parts[2], 1);
            }
            if ($filename_parts[1] == '758473') {
                $this->internal_number = '864';
                $this->external_number = substr($filename_parts[2], 1);
            }
            $this->called_date = $filename_parts[3];
            $this->called_time = $filename_parts[4];
        }
    }

    public function getStartTime(string $department) : string
    {
    	return (new \DateTime(
            $this->called_date . $this->called_time,
            new \DateTimeZone(CFG['timezone'][$department]))
            )->format(\DateTime::ATOM);
    }

    public function getEndTime(string $department) : string
    {
        $end_time = (new \DateTime(
            $this->called_date . $this->called_time,
            new \DateTimeZone(CFG['timezone'][$department]))
            )->modify('+1 hour');
        return $end_time->format(\DateTime::ATOM);
    }
}