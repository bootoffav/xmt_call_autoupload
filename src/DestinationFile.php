<?php

declare(strict_types=1);


namespace bootoffav\XMT\call_autoupload;

class DestinationFile
{
    use Curl;

    /**
     * array of folders ID from bitrix
     */
    protected $destinations = [];
    public $destination_folder;

    protected function folder_exists(string $folder_name, string $current_folder) : array
    {
        $folder_children = self::curlRequest(CFG['hostname'] . '/rest/disk.folder.getchildren?auth='
                                      . CallActivity::$authkey . '&id=' . $current_folder);
        foreach ($folder_children as $child) {
            if ($child->TYPE === 'folder' && $child->NAME == $folder_name) {
                return [ 'exists' => true, 'folder_id' => $child->ID];
            }
        }
        return [ 'exists' => false, 'folder_id' => null];
    }

    public function findFolder(string $called_date, string $current_folder) : string
    {
        if (array_key_exists($called_date, $this->destinations)) {
            return $this->destinations[$called_date];
        }
        $date = [
            'year' => substr($called_date, 0, 4),
            'month' => strftime('%B', strtotime($called_date)), //transforms 10 to October, 11 to November, etc.
            'mday' => substr($called_date, -2)
        ];

        foreach($date as $period) {
            $check_for_existence = $this->folder_exists($period, $current_folder);

            if ($check_for_existence['exists']) {
                $current_folder = $check_for_existence['folder_id'];
            }
            if ($check_for_existence['exists'] === false) {
                $response = self::curlRequest(CFG['hostname'] . '/rest/disk.folder.addsubfolder?auth='
                                              . CallActivity::$authkey
                                              . '&id=' . $current_folder
                                              . "&data[NAME]=$period");
                $current_folder = (string) $response->ID;
            }
        }
        return $this->destinations[$called_date] = $current_folder;
    }

    public function moveFileToDstFolder(SourceFile $src_file)
    {
        $this->curlRequest(CFG['hostname'] . '/rest/disk.file.moveto?auth=' . CallActivity::$authkey . '&id='
                            . $this->getFileId($src_file->name)
                            . '&targetFolderId=' . $this->destination_folder);
    }
    
    protected function getFileId(string $name) : string
    {
        $response = $this->curlRequest(CFG['hostname'] . '/rest/disk.folder.getchildren?auth=' . CallActivity::$authkey. "&id=361118"); //361118 - apps_folder, where uploaded files are stored by default
        foreach ($response as $folder_entry) {
            if ($folder_entry->TYPE === 'file' && $folder_entry->NAME === $name.'.mp3') {
                return $folder_entry->ID;
            }
        }
    }

}