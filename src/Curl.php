<?php
declare(strict_types=1);

namespace bootoffav\XMT\call_autoupload;

/**
 *
 */
trait Curl
{
    protected static function curlRequest(string $url, bool $check = false)
    {
        $ch = curl_init();
        $response = [];
        if ($check) {
            echo $url, PHP_EOL;
        }
        do {
            $next = ( isset($request->next) ) ? $request->next : 0;
            curl_setopt($ch, CURLOPT_URL, $url."&start=$next");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $request = json_decode( (curl_exec($ch)) );
            if (isset($request->error)) {
                throw new \Exception($request->error, 1);
                exit;
            }
            if (is_int($request->result)) {
                return $request;
            }
            $response = $request->result;
        } while (isset($request->next));

        curl_close($ch);
        return $response;
    }
}
