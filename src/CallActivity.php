<?php
declare(strict_types=1);

namespace bootoffav\XMT\call_autoupload;

class CallActivity
{
    use Curl;
    public static $authkey;
    public $responsible_employee;
    protected $start_time;
    protected $end_time;

    public function __construct(SourceFile $src_file)
    {
        $file = fopen('authkey', 'r', true);
        static::$authkey = fgets($file);
        fclose($file);
        $this->responsible_employee = $this->getEmployeeByInternalPhone($src_file->internal_number);
        $this->start_time = $src_file->getStartTime($this->responsible_employee['department_id']);
        $this->end_time = $src_file->getEndTime($this->responsible_employee['department_id']);
    }

    public function create(SourceFile $src_file) : bool
    {
        $crm_entity = $this->getCRMEntity($src_file->external_number) ?: [ 'id' => '17700', 'type_id' => '4']; //fallback if there is no CRM entity
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, CFG['hostname'] . '/rest/crm.activity.add');
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, [
            'auth'                             => static::$authkey,
            'fields[OWNER_ID]'                 => $crm_entity['id'],//find real Owner by phone number
            'fields[OWNER_TYPE_ID]'            => $crm_entity['type_id'],
            'fields[TYPE_ID]'                  => '2', //Call activity
            // 'fields[SUBJECT]'                  => $src_file->call_direction === '2' ? 
            //                                         "Call: {$src_file->duration['min']}m {$src_file->duration['sec']}s"
            //                                         : 'Call',
            'fields[SUBJECT]'                  => "Call: {$src_file->duration['min']}m {$src_file->duration['sec']}s",
            'fields[COMPLETED]'                => 'Y',
            'fields[START_TIME]'               => $this->start_time,
            'fields[END_TIME]'                 => $this->end_time,
            'fields[RESPONSIBLE_ID]'           => $this->responsible_employee['id'], //person who really called
            'fields[DIRECTION]'                => $src_file->call_direction,
            'fields[COMMUNICATIONS][0][VALUE]' => $src_file->external_number,
            'fields[FILES][0][fileData][0]'    => $src_file->name.'.mp3',
            'fields[FILES][0][fileData][1]'    => base64_encode(file_get_contents($src_file->dir . $src_file->name . '.mp3'))
        ]);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $out = json_decode(curl_exec($curl));
        curl_close($curl);
        if (property_exists($out, 'error')) {
            return false;
        }
        return true;
    }

    protected function getCRMEntity(string $external_number) : array
    {
        $type_id = [
            'contact' => '3',
            'company' => '4',
            'lead'    => '1'
        ];
        foreach(['company', 'contact', 'lead'] as $entity_type) {
            $response = $this->curlRequest(CFG['hostname'] . "/rest/crm.$entity_type.list?auth=" . static::$authkey . "&&filter[PHONE]=$external_number&select[0]=ID");
            if (count($response) > 0) {
                return [
                    'id' => $response[0]->ID,
                    'type_id' => $type_id[$entity_type]
                ];
            }
        }
        return [];
    }

    protected function getEmployeeByInternalPhone(string $internal_number) : array
    {

        $response = $this->curlRequest(CFG['hostname'] . '/rest/user.get?auth=' . static::$authkey . "&UF_PHONE_INNER={$internal_number}");
        return [ 'id' => (string) $response[0]->ID, 'department_id'=> (string) $response[0]->UF_DEPARTMENT[0]];
    }

}
