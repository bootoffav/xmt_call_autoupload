<?php

namespace bootoffav\XMT\call_autoupload;

class Buffer
{
    public static function readAll()
    {
        return file('buffer', FILE_IGNORE_NEW_LINES);
    }

    public static function flush()
    {
        file_put_contents('buffer', '');
    }

    public static function write($filename)
    {
        file_put_contents('buffer', $filename."\n", FILE_APPEND);
    }

    public static final function writeErrors($out, $filepath)
    {
        file_put_contents('errors',
                        'error: ' . $out->error
                        . ' , description: '. $out->error_description
                        .' -> ' . $filepath . "\n", FILE_APPEND);
    }
}
