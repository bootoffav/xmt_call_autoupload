<?php

declare(strict_types=1);

namespace bootoffav\XMT\call_autoupload;

return [
	'hostname' => 'https://xmtextiles.bitrix24.ru',
	'ats_folder' => '/var/spool/asterisk/monitor/',
	// keys are UF_DEPARTMENT in bitrix24
	'root_folder' => [
		'89'   => '365342',
		'8506' => '365342',
		'213'  => '349776',
		'8496' => '449150',
		'8470' => '449152',
		'8417' => '449152'
	],
	'timezone' => [
		'89'   => 'Europe/Vilnius',
		'8506' => 'Europe/Vilnius',
		'213'  => 'Asia/Shanghai',
		'8496' => 'Europe/Bucharest',
		'8470' => 'Europe/Madrid',
		'8417' => 'Europe/Madrid',
	]
];